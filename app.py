import urllib.parse
import threading
import requests
import hashlib
import pymongo
import logging
import hmac
import json
import os

from base64 import b64encode

from pymongo import MongoClient
from pymongo.errors import OperationFailure
from pymongo.errors import ServerSelectionTimeoutError

from classes.Mesh import Mesh

from flask import Flask, request, render_template, redirect, make_response, jsonify
app = Flask(__name__)

OG_SLACK_SIGNING_SECRET = bytes('9990371246812c1c0366c8ff61b89741', 'utf-8')
CN_SLACK_SIGNING_SECRET = bytes('0b7d2fd53d90adec8b1d3ddc7a6c054c', 'utf-8')

def verify_request(req):
	"""
	desc: Determines if a Slack request is authentic.
	params:
		`req` - Flask request object.
	"""

	request_ts = req.headers['X-Slack-Request-Timestamp']
	request_body = urllib.parse.urlencode(req.form)
	base_string = f"v0:{request_ts}:{request_body}".encode('utf-8')
	
	og_signature = "v0=" + hmac.new(OG_SLACK_SIGNING_SECRET, base_string, hashlib.sha256).hexdigest()
	cn_signature = "v0=" + hmac.new(CN_SLACK_SIGNING_SECRET, base_string, hashlib.sha256).hexdigest()

	if hmac.compare_digest(og_signature, req.headers['X-Slack-Signature']) or hmac.compare_digest(cn_signature, req.headers['X-Slack-Signature']):
		return True
	return False

@app.route('/', methods=['GET'])
def home():
	return render_template('home.html')

@app.route('/shiekh/', methods=['GET'])
def handle_shiekh():
	return render_template('shiekh.html')

@app.route('/footlocker/', methods=['GET'])
def handle_footlocker():
	return render_template('footlocker.html')

@app.route('/slam/', methods=['GET'])
def handle_slam():
	return render_template('slam.html')

@app.route('/prodirect/', methods=['GET'])
def handle_prodirect():
	return render_template('prodirect.html')

@app.route('/autofill/', methods=['GET'])
def autofill():
	return render_template('autofill.html')

@app.route('/qt/', methods=['GET'])
def qt():
	return render_template('shopify.html')

@app.route('/matches/', methods=['GET'])
def matches():
	return render_template('matches.html')

@app.route('/solebox/', methods=['GET'])
def solebox():
	return render_template('solebox.html')

@app.route('/snipes/', methods=['GET'])
def snipes():
	return render_template('snipes.html')

@app.route('/supply/', methods=['GET'])
def supply():
	return render_template('supply.html')

@app.route('/airness/', methods=['GET'])
def airness():
	return render_template('airness.html')

@app.route('/events/', methods=['POST'])
def handle_events():

	if not verify_request(request):
		return "Illegitimate request."

	event_data = json.loads(request.form['payload'])

	if event_data['callback_id'] == "check_stock":
		store, product_id = event_data['original_message']['attachments'][1]['actions'][0]['value'].split('|')
		t = threading.Thread(target=Mesh.check_stock, args=[store, product_id, event_data['response_url']])
		t.start()

	return ""

@app.route('/cncpts/', methods=['GET'])
def handle_cncpts():

	headers = {
		'content-type': 'application/graphql',
		'accept': 'application/json',
		'x-query-tag': '3415dbe059deb6b77b53c97cbac61f18',
		'x-sdk-variant': 'ios',
		'accept-encoding': 'gzip, deflate',
		'x-shopify-storefront-access-token': '92c74f05b306b31480ab114fb16961ea',
		'accept-language': '',
		'x-sdk-version': '4.0.0',
		'user-agent': 'Mobile Buy SDK iOS/4.0.0/com.tapcart.ftu2WnNaR'
	}

	variant = request.url.split('=')[-1]
	variant_id = b64encode(
		f"gid://shopify/ProductVariant/{variant}".encode()
	).decode()

	payload = """
		mutation {
			checkoutCreate(
				input: {
				lineItems: [
					{
					customAttributes: []
					quantity: 1
					variantId: "VARIANT_ID"
					}
				]
				note: null
				allowPartialAddresses: true
				presentmentCurrencyCode: USD
				}
			) {
				checkout {
				id
				ready
				requiresShipping
				taxesIncluded
				email
				discountApplications(first: 250) {
					edges {
					node {
						__typename
						... on DiscountCodeApplication {
						applicable
						code
						targetType
						}
						... on ManualDiscountApplication {
						title
						}
						... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
						}
					}
					}
				}
				shippingDiscountAllocations {
					allocatedAmount {
					amount
					currencyCode
					}
					discountApplication {
					__typename
					... on DiscountCodeApplication {
						applicable
						code
						targetType
					}
					... on ManualDiscountApplication {
						title
					}
					... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
					}
					}
				}
				appliedGiftCards {
					id
					balanceV2 {
					amount
					currencyCode
					}
					lastCharacters
					presentmentAmountUsed {
					amount
					currencyCode
					}
				}
				shippingAddress {
					id
					firstName
					lastName
					phone
					address1
					address2
					city
					country
					countryCodeV2
					province
					provinceCode
					zip
					company
				}
				shippingLine {
					handle
					title
					priceV2 {
					amount
					currencyCode
					}
				}
				note
				lineItems(first: 250) {
					edges {
					cursor
					node {
						variant {
						id
						image {
							altText
							transformedSrc
						}
						presentmentPrices(presentmentCurrencies: [USD], first: 1) {
							edges {
							node {
								price {
								amount
								currencyCode
								}
								compareAtPrice {
								amount
								currencyCode
								}
							}
							}
						}
						}
						title
						quantity
						discountAllocations {
						allocatedAmount {
							amount
							currencyCode
						}
						discountApplication {
							__typename
							... on DiscountCodeApplication {
							applicable
							code
							targetType
							}
							... on ManualDiscountApplication {
							title
							}
							... on ScriptDiscountApplication {
							title
							targetType
							targetSelection
							allocationMethod
							}
						}
						}
						customAttributes {
						key
						value
						}
					}
					}
				}
				webUrl
				currencyCode
				subtotalPriceV2 {
					amount
					currencyCode
				}
				totalTaxV2 {
					amount
					currencyCode
				}
				totalPriceV2 {
					amount
					currencyCode
				}
				paymentDueV2 {
					amount
					currencyCode
				}
				order {
					id
					financialStatus
				}
				}
				checkoutUserErrors {
				field
				message
				}
			}
		}
	""".replace('VARIANT_ID', variant_id)

	try:
		res = requests.post(
			"https://conceptsintl.myshopify.com/api/2020-04/graphql",
			headers=headers, data=payload, timeout=5
		)

		checkout_url = res.json()['data']['checkoutCreate']['checkout']['webUrl']

		return redirect(checkout_url, code=302)
		
	except:
		return "Welp, that wasn't suppose to happen."

@app.route('/tbg/', methods=['GET'])
def handle_tbg():

	headers = {
		'content-type': 'application/graphql',
		'accept': 'application/json',
		'x-query-tag': '2a6b3a25c7535b040262b60df22d4319',
		'x-sdk-variant': 'ios',
		'accept-encoding': 'gzip, deflate',
		'x-shopify-storefront-access-token': 'd72ab583d5803cac2501ef4a90bdb3a3',
		'accept-language': '',
		'x-sdk-version': '4.0.0',
		'user-agent': 'Mobile Buy SDK iOS/4.0.0/com.tapcart.ftu2WnNaR'
	}

	variant = request.url.split('=')[-1]
	variant_id = b64encode(
		f"gid://shopify/ProductVariant/{variant}".encode()
	).decode()

	payload = """
		mutation {
			checkoutCreate(
				input: {
				lineItems: [
					{
					customAttributes: []
					quantity: 1
					variantId: "VARIANT_ID"
					}
				]
				note: null
				allowPartialAddresses: true
				presentmentCurrencyCode: USD
				}
			) {
				checkout {
				id
				ready
				requiresShipping
				taxesIncluded
				email
				discountApplications(first: 250) {
					edges {
					node {
						__typename
						... on DiscountCodeApplication {
						applicable
						code
						targetType
						}
						... on ManualDiscountApplication {
						title
						}
						... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
						}
					}
					}
				}
				shippingDiscountAllocations {
					allocatedAmount {
					amount
					currencyCode
					}
					discountApplication {
					__typename
					... on DiscountCodeApplication {
						applicable
						code
						targetType
					}
					... on ManualDiscountApplication {
						title
					}
					... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
					}
					}
				}
				appliedGiftCards {
					id
					balanceV2 {
					amount
					currencyCode
					}
					lastCharacters
					presentmentAmountUsed {
					amount
					currencyCode
					}
				}
				shippingAddress {
					id
					firstName
					lastName
					phone
					address1
					address2
					city
					country
					countryCodeV2
					province
					provinceCode
					zip
					company
				}
				shippingLine {
					handle
					title
					priceV2 {
					amount
					currencyCode
					}
				}
				note
				lineItems(first: 250) {
					edges {
					cursor
					node {
						variant {
						id
						image {
							altText
							transformedSrc
						}
						presentmentPrices(presentmentCurrencies: [USD], first: 1) {
							edges {
							node {
								price {
								amount
								currencyCode
								}
								compareAtPrice {
								amount
								currencyCode
								}
							}
							}
						}
						}
						title
						quantity
						discountAllocations {
						allocatedAmount {
							amount
							currencyCode
						}
						discountApplication {
							__typename
							... on DiscountCodeApplication {
							applicable
							code
							targetType
							}
							... on ManualDiscountApplication {
							title
							}
							... on ScriptDiscountApplication {
							title
							targetType
							targetSelection
							allocationMethod
							}
						}
						}
						customAttributes {
						key
						value
						}
					}
					}
				}
				webUrl
				currencyCode
				subtotalPriceV2 {
					amount
					currencyCode
				}
				totalTaxV2 {
					amount
					currencyCode
				}
				totalPriceV2 {
					amount
					currencyCode
				}
				paymentDueV2 {
					amount
					currencyCode
				}
				order {
					id
					financialStatus
				}
				}
				checkoutUserErrors {
				field
				message
				}
			}
		}
	""".replace('VARIANT_ID', variant_id)

	try:
		res = requests.post(
			"https://thebettergeneration.myshopify.com/api/2020-04/graphql",
			headers=headers, data=payload, timeout=5
		)

		checkout_url = res.json()['data']['checkoutCreate']['checkout']['webUrl']

		return redirect(checkout_url, code=302)
		
	except:
		return "Welp, that wasn't suppose to happen."

@app.route('/bbb/', methods=['GET'])
def handle_bbb():

	headers = {
		'content-type': 'application/graphql',
		'accept': 'application/json',
		'x-sdk-variant': 'ios',
		'accept-encoding': 'gzip, deflate',
		'x-shopify-storefront-access-token': 'e079bb47388fe620590b99c8abad5511',
		'accept-language': '',
		'x-sdk-version': '4.0.0',
		'user-agent': 'Mobile Buy SDK iOS/4.0.0/com.tapcart.ftu2WnNaR'
	}

	variant = request.url.split('=')[-1]
	variant_id = b64encode(
		f"gid://shopify/ProductVariant/{variant}".encode()
	).decode()

	payload = """
		mutation {
			checkoutCreate(
				input: {
				lineItems: [
					{
					customAttributes: []
					quantity: 1
					variantId: "VARIANT_ID"
					}
				]
				note: null
				allowPartialAddresses: true
				presentmentCurrencyCode: USD
				}
			) {
				checkout {
				id
				ready
				requiresShipping
				taxesIncluded
				email
				discountApplications(first: 250) {
					edges {
					node {
						__typename
						... on DiscountCodeApplication {
						applicable
						code
						targetType
						}
						... on ManualDiscountApplication {
						title
						}
						... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
						}
					}
					}
				}
				shippingDiscountAllocations {
					allocatedAmount {
					amount
					currencyCode
					}
					discountApplication {
					__typename
					... on DiscountCodeApplication {
						applicable
						code
						targetType
					}
					... on ManualDiscountApplication {
						title
					}
					... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
					}
					}
				}
				appliedGiftCards {
					id
					balanceV2 {
					amount
					currencyCode
					}
					lastCharacters
					presentmentAmountUsed {
					amount
					currencyCode
					}
				}
				shippingAddress {
					id
					firstName
					lastName
					phone
					address1
					address2
					city
					country
					countryCodeV2
					province
					provinceCode
					zip
					company
				}
				shippingLine {
					handle
					title
					priceV2 {
					amount
					currencyCode
					}
				}
				note
				lineItems(first: 250) {
					edges {
					cursor
					node {
						variant {
						id
						image {
							altText
							transformedSrc
						}
						presentmentPrices(presentmentCurrencies: [USD], first: 1) {
							edges {
							node {
								price {
								amount
								currencyCode
								}
								compareAtPrice {
								amount
								currencyCode
								}
							}
							}
						}
						}
						title
						quantity
						discountAllocations {
						allocatedAmount {
							amount
							currencyCode
						}
						discountApplication {
							__typename
							... on DiscountCodeApplication {
							applicable
							code
							targetType
							}
							... on ManualDiscountApplication {
							title
							}
							... on ScriptDiscountApplication {
							title
							targetType
							targetSelection
							allocationMethod
							}
						}
						}
						customAttributes {
						key
						value
						}
					}
					}
				}
				webUrl
				currencyCode
				subtotalPriceV2 {
					amount
					currencyCode
				}
				totalTaxV2 {
					amount
					currencyCode
				}
				totalPriceV2 {
					amount
					currencyCode
				}
				paymentDueV2 {
					amount
					currencyCode
				}
				order {
					id
					financialStatus
				}
				}
				checkoutUserErrors {
				field
				message
				}
			}
		}
	""".replace('VARIANT_ID', variant_id)

	try:
		res = requests.post(
			"https://bb-branded.myshopify.com/api/2020-04/graphql",
			headers=headers, data=payload, timeout=5
		)

		checkout_url = res.json()['data']['checkoutCreate']['checkout']['webUrl']

		return redirect(checkout_url, code=302)

	except:
		return "Welp, that wasn't suppose to happen."

@app.route('/butter/', methods=['GET'])
def handle_butter():

	headers = {
		'content-type': 'application/graphql',
		'accept': 'application/json',
		'x-sdk-variant': 'ios',
		'accept-encoding': 'gzip, deflate',
		'x-shopify-storefront-access-token': '95c487b045b0b83a762c218eafb49363',
		'accept-language': '',
		'x-sdk-version': '4.0.0',
		'user-agent': 'Mobile Buy SDK iOS/4.0.0/com.tapcart.ftu2WnNaR'
	}

	variant = request.url.split('=')[-1]
	variant_id = b64encode(
		f"gid://shopify/ProductVariant/{variant}".encode()
	).decode()

	payload = """
		mutation {
			checkoutCreate(
				input: {
				lineItems: [
					{
					customAttributes: []
					quantity: 1
					variantId: "VARIANT_ID"
					}
				]
				note: null
				allowPartialAddresses: true
				presentmentCurrencyCode: USD
				}
			) {
				checkout {
				id
				ready
				requiresShipping
				taxesIncluded
				email
				discountApplications(first: 250) {
					edges {
					node {
						__typename
						... on DiscountCodeApplication {
						applicable
						code
						targetType
						}
						... on ManualDiscountApplication {
						title
						}
						... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
						}
					}
					}
				}
				shippingDiscountAllocations {
					allocatedAmount {
					amount
					currencyCode
					}
					discountApplication {
					__typename
					... on DiscountCodeApplication {
						applicable
						code
						targetType
					}
					... on ManualDiscountApplication {
						title
					}
					... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
					}
					}
				}
				appliedGiftCards {
					id
					balanceV2 {
					amount
					currencyCode
					}
					lastCharacters
					presentmentAmountUsed {
					amount
					currencyCode
					}
				}
				shippingAddress {
					id
					firstName
					lastName
					phone
					address1
					address2
					city
					country
					countryCodeV2
					province
					provinceCode
					zip
					company
				}
				shippingLine {
					handle
					title
					priceV2 {
					amount
					currencyCode
					}
				}
				note
				lineItems(first: 250) {
					edges {
					cursor
					node {
						variant {
						id
						image {
							altText
							transformedSrc
						}
						presentmentPrices(presentmentCurrencies: [USD], first: 1) {
							edges {
							node {
								price {
								amount
								currencyCode
								}
								compareAtPrice {
								amount
								currencyCode
								}
							}
							}
						}
						}
						title
						quantity
						discountAllocations {
						allocatedAmount {
							amount
							currencyCode
						}
						discountApplication {
							__typename
							... on DiscountCodeApplication {
							applicable
							code
							targetType
							}
							... on ManualDiscountApplication {
							title
							}
							... on ScriptDiscountApplication {
							title
							targetType
							targetSelection
							allocationMethod
							}
						}
						}
						customAttributes {
						key
						value
						}
					}
					}
				}
				webUrl
				currencyCode
				subtotalPriceV2 {
					amount
					currencyCode
				}
				totalTaxV2 {
					amount
					currencyCode
				}
				totalPriceV2 {
					amount
					currencyCode
				}
				paymentDueV2 {
					amount
					currencyCode
				}
				order {
					id
					financialStatus
				}
				}
				checkoutUserErrors {
				field
				message
				}
			}
		}
	""".replace('VARIANT_ID', variant_id)

	try:
		res = requests.post(
			"https://extrabutterny.myshopify.com/api/2020-04/graphql",
			headers=headers, data=payload, timeout=5
		)

		checkout_url = res.json()['data']['checkoutCreate']['checkout']['webUrl']

		return redirect(checkout_url, code=302)

	except:
		return "Welp, that wasn't suppose to happen."

@app.route('/patta/', methods=['GET'])
def handle_patta():

	headers = {
		'content-type': 'application/graphql',
		'accept': 'application/json',
		'x-sdk-variant': 'ios',
		'accept-encoding': 'gzip, deflate',
		'x-shopify-storefront-access-token': '48aa0f2e6fae4b8294a37dbadc7e0046',
		'accept-language': '',
		'x-sdk-version': '4.0.0',
		'user-agent': 'Mobile Buy SDK iOS/4.0.0/com.tapcart.ftu2WnNaR'
	}

	variant = request.url.split('=')[-1]
	variant_id = b64encode(
		f"gid://shopify/ProductVariant/{variant}".encode()
	).decode()

	payload = """
		mutation {
			checkoutCreate(
				input: {
				lineItems: [
					{
					customAttributes: []
					quantity: 1
					variantId: "VARIANT_ID"
					}
				]
				note: null
				allowPartialAddresses: true
				presentmentCurrencyCode: USD
				}
			) {
				checkout {
				id
				ready
				requiresShipping
				taxesIncluded
				email
				discountApplications(first: 250) {
					edges {
					node {
						__typename
						... on DiscountCodeApplication {
						applicable
						code
						targetType
						}
						... on ManualDiscountApplication {
						title
						}
						... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
						}
					}
					}
				}
				shippingDiscountAllocations {
					allocatedAmount {
					amount
					currencyCode
					}
					discountApplication {
					__typename
					... on DiscountCodeApplication {
						applicable
						code
						targetType
					}
					... on ManualDiscountApplication {
						title
					}
					... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
					}
					}
				}
				appliedGiftCards {
					id
					balanceV2 {
					amount
					currencyCode
					}
					lastCharacters
					presentmentAmountUsed {
					amount
					currencyCode
					}
				}
				shippingAddress {
					id
					firstName
					lastName
					phone
					address1
					address2
					city
					country
					countryCodeV2
					province
					provinceCode
					zip
					company
				}
				shippingLine {
					handle
					title
					priceV2 {
					amount
					currencyCode
					}
				}
				note
				lineItems(first: 250) {
					edges {
					cursor
					node {
						variant {
						id
						image {
							altText
							transformedSrc
						}
						presentmentPrices(presentmentCurrencies: [USD], first: 1) {
							edges {
							node {
								price {
								amount
								currencyCode
								}
								compareAtPrice {
								amount
								currencyCode
								}
							}
							}
						}
						}
						title
						quantity
						discountAllocations {
						allocatedAmount {
							amount
							currencyCode
						}
						discountApplication {
							__typename
							... on DiscountCodeApplication {
							applicable
							code
							targetType
							}
							... on ManualDiscountApplication {
							title
							}
							... on ScriptDiscountApplication {
							title
							targetType
							targetSelection
							allocationMethod
							}
						}
						}
						customAttributes {
						key
						value
						}
					}
					}
				}
				webUrl
				currencyCode
				subtotalPriceV2 {
					amount
					currencyCode
				}
				totalTaxV2 {
					amount
					currencyCode
				}
				totalPriceV2 {
					amount
					currencyCode
				}
				paymentDueV2 {
					amount
					currencyCode
				}
				order {
					id
					financialStatus
				}
				}
				checkoutUserErrors {
				field
				message
				}
			}
		}
	""".replace('VARIANT_ID', variant_id)

	try:
		res = requests.post(
			"https://pattanl.myshopify.com/api/2020-01/graphql",
			headers=headers, data=payload, timeout=5
		)

		checkout_url = res.json()['data']['checkoutCreate']['checkout']['webUrl']

		return redirect(checkout_url, code=302)

	except:
		return "Welp, that wasn't suppose to happen."

@app.route('/nrml/', methods=['GET'])
def handle_nrml():

	headers = {
		'content-type': 'application/graphql',
		'accept': 'application/json',
		'x-sdk-variant': 'ios',
		'accept-encoding': 'gzip, deflate',
		'x-shopify-storefront-access-token': 'd28fed92fb8b65e36c6c399ad23ecc4f',
		'accept-language': '',
		'x-sdk-version': '4.0.0',
		'user-agent': 'Mobile Buy SDK iOS/4.0.0/com.tapcart.ftu2WnNaR'
	}

	variant = request.url.split('=')[-1]
	variant_id = b64encode(
		f"gid://shopify/ProductVariant/{variant}".encode()
	).decode()

	payload = """
		mutation {
			checkoutCreate(
				input: {
				lineItems: [
					{
					customAttributes: []
					quantity: 1
					variantId: "VARIANT_ID"
					}
				]
				note: null
				allowPartialAddresses: true
				presentmentCurrencyCode: USD
				}
			) {
				checkout {
				id
				ready
				requiresShipping
				taxesIncluded
				email
				discountApplications(first: 250) {
					edges {
					node {
						__typename
						... on DiscountCodeApplication {
						applicable
						code
						targetType
						}
						... on ManualDiscountApplication {
						title
						}
						... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
						}
					}
					}
				}
				shippingDiscountAllocations {
					allocatedAmount {
					amount
					currencyCode
					}
					discountApplication {
					__typename
					... on DiscountCodeApplication {
						applicable
						code
						targetType
					}
					... on ManualDiscountApplication {
						title
					}
					... on ScriptDiscountApplication {
						title
						targetType
						targetSelection
						allocationMethod
					}
					}
				}
				appliedGiftCards {
					id
					balanceV2 {
					amount
					currencyCode
					}
					lastCharacters
					presentmentAmountUsed {
					amount
					currencyCode
					}
				}
				shippingAddress {
					id
					firstName
					lastName
					phone
					address1
					address2
					city
					country
					countryCodeV2
					province
					provinceCode
					zip
					company
				}
				shippingLine {
					handle
					title
					priceV2 {
					amount
					currencyCode
					}
				}
				note
				lineItems(first: 250) {
					edges {
					cursor
					node {
						variant {
						id
						image {
							altText
							transformedSrc
						}
						presentmentPrices(presentmentCurrencies: [USD], first: 1) {
							edges {
							node {
								price {
								amount
								currencyCode
								}
								compareAtPrice {
								amount
								currencyCode
								}
							}
							}
						}
						}
						title
						quantity
						discountAllocations {
						allocatedAmount {
							amount
							currencyCode
						}
						discountApplication {
							__typename
							... on DiscountCodeApplication {
							applicable
							code
							targetType
							}
							... on ManualDiscountApplication {
							title
							}
							... on ScriptDiscountApplication {
							title
							targetType
							targetSelection
							allocationMethod
							}
						}
						}
						customAttributes {
						key
						value
						}
					}
					}
				}
				webUrl
				currencyCode
				subtotalPriceV2 {
					amount
					currencyCode
				}
				totalTaxV2 {
					amount
					currencyCode
				}
				totalPriceV2 {
					amount
					currencyCode
				}
				paymentDueV2 {
					amount
					currencyCode
				}
				order {
					id
					financialStatus
				}
				}
				checkoutUserErrors {
				field
				message
				}
			}
		}
	""".replace('VARIANT_ID', variant_id)

	try:
		res = requests.post(
			"https://nrmlstreetwear.myshopify.com/api/2021-01/graphql",
			headers=headers, data=payload, timeout=5
		)

		checkout_url = res.json()['data']['checkoutCreate']['checkout']['webUrl']

		return redirect(checkout_url, code=302)

	except:
		return "Welp, that wasn't suppose to happen."

@app.route('/update_keywords/', methods=['GET'])
def update_keywords():

	# Establish DB collection
	client = MongoClient("mongodb+srv://alan:80cAzW0TpUYsO5hQ@cluster0-uxfuv.mongodb.net/test")
	db = client['dynamic-kw']
	collection = db['keywords']
	cursor = collection.find({})

	# Clear all keywords from pos and neg keyword lists to be updated
	pos_keywords = open('keywords/pos_keywords.txt', 'w+')
	pos_keywords.truncate(0)
	neg_keywords = open('keywords/neg_keywords.txt', 'w+')
	neg_keywords.truncate(0)

	# Pull all keywords from collection and put into correct list depending on positive or not.
	for document in cursor:
		for pos_keyword in document['positive_keywords']:
			pos_keywords.write(f"{pos_keyword}\n")
		for neg_keyword in document['negative_keywords']:
			neg_keywords.write(f"{neg_keyword}\n")

	client.close()
	pos_keywords.close()
	neg_keywords.close()

	return make_response(jsonify({
		"message": "Successfully pulled new keywords and stored them in keywords files."
	}), 200)

@app.route('/get_keywords/', methods=['GET'])
def get_keywords():

	# Open positive aand negative key word files
	if not os.path.exists("keywords/pos_keywords.txt"):
		return make_response(jsonify({"Message": "Positive keyword file not found!"}), 400)

	with open("keywords/pos_keywords.txt") as positive_keywords:
		pos_keywords = { pos_keyword.strip().lower() for pos_keyword in positive_keywords }

	if not os.path.exists("keywords/neg_keywords.txt"):
		return make_response(jsonify({"Message": "Negative keyword file not found!"}), 400)

	with open("keywords/pos_keywords.txt") as positive_keywords:
		pos_keywords = { pos_keyword.strip().lower() for pos_keyword in positive_keywords }

	with open("keywords/neg_keywords.txt") as negative_keywords:
		neg_keywords = { neg_keyword.strip().lower() for neg_keyword in negative_keywords }

	pos_keywords = list(pos_keywords)
	neg_keywords = list(neg_keywords)

	return make_response(jsonify({
		"message": "Keywords have been successfully returned.",
		"positive": pos_keywords,
		"negative": neg_keywords
	}), 200)

@app.route('/nike_atc/', methods=['POST'])
def nike_atc():

	data = request.get_json()

	try:
		headers = {
			'accept': 'application/json',
			'content-type': 'application/json; charset=UTF-8',
			'appid': 'com.nike.commerce.nikedotcom.web',
			'x-nike-visitorid': data['visitor_id'],
			'x-nike-visitid': '1',
		}

	except:
		return make_response(jsonify({
			"message": "Header parsing failed."
		}), 400)

	try:
		payload = [{
			"op": "add",
			"path": "/items",
			"value": {
				"skuId": data['sku'],
				"quantity": 1
			}
		}]
		
	except:
		return make_response(jsonify({
			"message": "Payload parsing failed."
		}), 400)

	proxy = {
		'http': 'http://e0f4Z4YbBX:27eWk9zylm@154.36.87.48:16964',
		'https': 'http://e0f4Z4YbBX:27eWk9zylm@154.36.87.48:16964'
	}

	try:
		response = requests.patch(
			f"https://api.nike.com/buy/carts/v2/{data['region']}/NIKE/NIKECOM;.css?modifiers=VALIDATELIMITS,VALIDATEAVAILABILITY",
			headers=headers, json=payload, proxies=proxy, timeout=15
		)

		if response.status_code == 200:
			return make_response(jsonify({
				"message": "Product has been added to cart."
			}), 200)

		else:
			return make_response(jsonify({
				"message": "Product was not added to cart."
			}), 400)

	except:
		return make_response(jsonify({
			"message": "Request failed."
		}), 400)

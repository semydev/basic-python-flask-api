import requests
import uuid

store_credentials = {
    'footpatrol': {
        'color': '#2ee36e',
        'api_key': '5893BD6BE6324C49A73E7E97ED6C36D1',
        'mesh_url': 'https://prod.jdgroupmesh.cloud/stores/footpatrolgb/products',
        'user_agent': 'footpatrolgb/6.3.2.1446 (ipad-app; iOS 10.3.3)'
    },
    'size': {
        'color': '#ff8735',
        'api_key': '7EA429CEC4C34332B878B754E7BE20B5',
        'mesh_url': 'https://prod.jdgroupmesh.cloud/stores/size/products',
        'user_agent': 'size/6.3.2.1443 (ipad-app; iOS 10.3.3)'
    },
    'jd_sports': {
        'color': '#000000',
        'api_key': '9D08E84E1D2D4BB89483407B46E252D2',
        'mesh_url': 'https://prod.jdgroupmesh.cloud/stores/jdsports/products',
        'user_agent': 'jdsports/6.4.3.1450 (ipad-app; iOS 10.3.3)'
    },
    'hip_store': {
        'color': '#2b4458',
        'api_key': '',  # TODO: Find this.
        'mesh_url': 'https://prod.jdgroupmesh.cloud/stores/thehipstore/products',
        'user_agent': 'thehipstore/6.3.5.1506 (ipad-app; iOS 10.3.3)'
    }
}

class Mesh:

    def __init__(self):
        pass

    @staticmethod
    def check_stock(store, product_id, response_url):
    
        store_data = store_credentials[store]

        headers = {
            'accept': 'application/vnd.cloudsearch.add.sdf.2013',
            'user-agent': 'curl/7.43.0'
        }

        params = {
            'api_key': store_data['api_key'],
            'channel': 'iphone-app',
            'expand': 'variations,informationBlocks,customisations'
        }

        try:
            uncached_url = f"{store_data['mesh_url']}/{product_id}?channel={str(uuid.uuid4())[:6]}"
            res = requests.get(uncached_url, headers=headers, params=params, timeout=10)

            if res.status_code == 200:
                if not res.json():
                    exit()

                r = res.json()

                requests.post(response_url, json={
                    "response_type": "ephemeral",
			        "replace_original": False,
                    "attachments": [
                        {
                            "color": store_data['color'],
                            "fields": [
                                {
                                    "title": "Product",
                                    "value": f"{r['fields']['name']}\n{r['fields']['sku']}",
                                    "short": True
                                },
                                {
                                    "title": "Total Stock",
                                    "value": r['fields']['quantity'],
                                    "short": True
                                }
                            ]
                        }
                    ]
                })

        except (requests.exceptions.ConnectionError, requests.exceptions.ProxyError, requests.exceptions.ReadTimeout):
            pass

        except Exception as e:
            print(e)
            pass
